package xyz.ilyaxabibullin.lessonshedule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv;
    WeekAdapter adapter;
    ArrayList<String> days = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv = findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(getBaseContext(),LinearLayoutManager.VERTICAL,false));
        fillWeek();
        adapter = new WeekAdapter(days);
        rv.setAdapter(adapter);
        adapter.setOnItemClickListener((v,i) ->{
            //Toast.makeText(MainActivity.this,i,Toast.LENGTH_SHORT).show();
            Log.d("ActivityMain", String.valueOf(i));
        });
    }

    private void fillWeek(){
        days.add("Monday");
        days.add("Monday");
        days.add("Monday");
        days.add("Monday");
        days.add("Monday");
        days.add("Monday");
        days.add("Monday");

    }
}
