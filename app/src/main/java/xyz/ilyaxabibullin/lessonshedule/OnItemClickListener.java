package xyz.ilyaxabibullin.lessonshedule;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View v, int position);
}
