package xyz.ilyaxabibullin.lessonshedule;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class WeekAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private OnItemClickListener onItemClickListener;


    ArrayList<String> days = new ArrayList<>();
    WeekAdapter(ArrayList<String> days){
        this.days = days;
    }

    public  void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item,viewGroup,false);
        return new DayHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder dayHolder, int i) {
        DayHolder dayHolder1 = (DayHolder) dayHolder;
        dayHolder1.textView.setText(days.get(i));
    }

    @Override
    public int getItemCount() {
        return days.size();
    }


    class DayHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView textView;
        LinearLayout ll;

        public DayHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.dayname);
            ll = itemView.findViewById(R.id.item);
            DayHolder dh = this;
            ll.setOnClickListener(dh);

        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }
}
